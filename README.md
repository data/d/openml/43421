# OpenML dataset: Game-of-Thrones-Script-All-Seasons

https://www.openml.org/d/43421

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Dataset is generated through a long and complex process. Starting from scrapping the whole URLs provided on Genius.com for Game of Thrones series. Process on scrapping and cleaning the dataset required a lot of time and effort in which I managed to utilize wide range of package available for collecting and compiling data scattered all over the internet.
This dataset is inspired by previous similar dataset published by Ander Fernndez Jauregui on https://www.kaggle.com/anderfj/game-of-thrones-series-scripts-breakdowns. I was waiting for him to update the dataset to do some analysis on them. Unfortunately, it was a long time since he last updated the dataset. Therefore, following some of his practice I generated this dataset, and hopefully will be a good use for anyone or at least for my personal analysis.
Content
The content inside is a complete set of Game of Thrones script for all seasons in form of a table containing 6 columns with different data types used for various purposes. Description on each columns are provided on the data description part.
Acknowledgements
Great credits for Genius.com to published the whole script of Game of Thrones series completely. Also, kudos to all of the open source packages out there, as well as people who are contributing on them so we can utilize those packages as we pleases.
Inspiration
There is only one question that I want to find answer using this dataset. Who is the true hero/heroin in the whole series?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43421) of an [OpenML dataset](https://www.openml.org/d/43421). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43421/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43421/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43421/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

